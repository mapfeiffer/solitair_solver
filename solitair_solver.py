import tkinter
import time
import _thread
import sys
import cProfile

class Field(object):
	def __init__(self):
		# German peg solitair
		self.f = [
			0,0,0,1,1,1,0,0,0,
			0,0,0,1,1,1,0,0,0,
			0,0,0,1,1,1,0,0,0,
			1,1,1,1,1,1,1,1,1,
			1,1,1,1,1,1,1,1,1,
			1,1,1,1,1,1,1,1,1,
			0,0,0,1,1,1,0,0,0,
			0,0,0,1,1,1,0,0,0,
			0,0,0,1,1,1,0,0,0
		]

		self.pos = [
			0,0,0,1,1,1,0,0,0,
			0,0,0,1,1,1,0,0,0,
			0,0,0,1,1,1,0,0,0,
			1,1,1,1,1,1,1,1,1,
			1,1,1,1,0,1,1,1,1,
			1,1,1,1,1,1,1,1,1,
			0,0,0,1,1,1,0,0,0,
			0,0,0,1,1,1,0,0,0,
			0,0,0,1,1,1,0,0,0
		]

		# English peg solitair
		#~ self.f = [
			#~ 0,0,0,0,0,0,0,0,0,
			#~ 0,0,0,1,1,1,0,0,0,
			#~ 0,0,0,1,1,1,0,0,0,
			#~ 0,1,1,1,1,1,1,1,0,
			#~ 0,1,1,1,1,1,1,1,0,
			#~ 0,1,1,1,1,1,1,1,0,
			#~ 0,0,0,1,1,1,0,0,0,
			#~ 0,0,0,1,1,1,0,0,0,
			#~ 0,0,0,0,0,0,0,0,0
		#~ ]
#~
		#~ self.pos = [
			#~ 0,0,0,0,0,0,0,0,0,
			#~ 0,0,0,1,1,1,0,0,0,
			#~ 0,0,0,1,1,1,0,0,0,
			#~ 0,1,1,1,1,1,1,1,0,
			#~ 0,1,1,1,0,1,1,1,0,
			#~ 0,1,1,1,1,1,1,1,0,
			#~ 0,0,0,1,1,1,0,0,0,
			#~ 0,0,0,1,1,1,0,0,0,
			#~ 0,0,0,0,0,0,0,0,0
		#~ ]

	def getHash(self):
		#return ''.join(map(str,self.pos))
		i=0
		b=0
		while i < len(self.pos):
			b = (b<<1) | self.pos[i]
			i=i+1

		return b

	def getTwoLevelHash(self):
		i = 0
		h1 = 0
		h2 = 0
		while i < 32:
			h1 = (h1<<1) | self.pos[i]
			i += 1
		while i < len(self.pos):
			h2 = (h2<<1) | self.pos[i]
			i=i+1

		return (h1, h2)


	def isOccupied(self, x, y):
		return self.pos[9 * x + y]

	def isField(self, x, y):
		return self.f[9 * x + y]

	def setPosition(self, x, y, value):
		self.pos[9 * x + y] = value

	def isValidMove(self, move):
		xDiff = move.x1 - move.x2
		yDiff = move.y1 - move.y2

		if not self.isField(move.x1, move.y1):
			return False

		if not self.isField(move.x2, move.y2):
			return False

		if abs(xDiff) != 2 and abs(yDiff) != 2:
			return False

		if move.x1 - move.x2 != 0 and move.y1 - move.y2 != 0:
			return False

		# start position
		if not self.isOccupied(move.x1, move.y1):
			return False

		# target position
		if self.isOccupied(move.x2, move.y2):
			return False

		if move.direction == 'x':
			if not self.isOccupied(move.x1 - abs(xDiff) // (xDiff), move.y1):
				return False
		elif move.direction == 'y':
			if not self.isOccupied(move.x1, move.y1 - abs(yDiff) // (yDiff)):
				return False

		return True

	def isSolvable(self):
		return True

	def getMoves(self):
		moves = []
		for x in range(9):
			for y in range(9):
				if self.isOccupied(x, y):
					#right
					if x <= 6:
						if self.isField(x+1, y) and self.isField(x+2, y):
							if self.isOccupied(x+1, y) and not self.isOccupied(x+2, y):
								moves.append(Move(x, y, x+2, y))

					#down
					if y <= 6:
						if self.isField(x, y+1) and self.isField(x, y+2):
							if self.isOccupied(x, y+1) and not self.isOccupied(x, y+2):
								moves.append(Move(x, y, x, y+2))

					#left
					if x >= 2:
						if self.isField(x-1, y) and self.isField(x-2, y):
							if self.isOccupied(x-1, y) and not self.isOccupied(x-2, y):
								moves.append(Move(x, y, x-2, y))

					#up
					if y >= 2:
						if self.isField(x, y-1) and self.isField(x, y-2):
							if self.isOccupied(x, y-1) and not self.isOccupied(x, y-2):
								moves.append(Move(x, y, x, y-2))

		return moves


	# unchecked move
	def move(self, move):
		xDiff = move.x1 - move.x2
		yDiff = move.y1 - move.y2

		#if self.isValidMove(move):
		self.setPosition(move.x1, move.y1, 0)
		if move.direction == 'x':
			self.setPosition(move.x1 - abs(xDiff) // (xDiff),
				move.y1,
				0)
		elif move.direction == 'y':
			self.setPosition(move.x1,
				move.y1 - abs(yDiff) // (yDiff),
				0)

		self.setPosition(move.x2, move.y2, 1)


	# unchecked reverse of a move
	def reverse(self, move):
		xDiff = move.x1 - move.x2
		yDiff = move.y1 - move.y2


		self.setPosition(move.x1, move.y1, 1)
		if move.direction == 'x':
			self.setPosition(move.x1 - abs(xDiff) // (xDiff),
				move.y1,
				1)
		elif move.direction == 'y':
			self.setPosition(move.x1,
				move.y1 - abs(yDiff) // (yDiff),
				1)

		self.setPosition(move.x2, move.y2, 0)


	def isComplete(self):
		return self.numberOfPieces() == 1


	def numberOfPieces(self):
		return sum(self.pos)


class Move(object):
	def __init__(self, x1, y1, x2, y2):
		self.x1 = x1
		self.y1 = y1
		self.x2 = x2
		self.y2 = y2

		if self.x1 - self.x2:
			self.direction = 'x'
		elif self.y1 - self.y2:
			self.direction = 'y'
		else:
			raise Exception('no direction')

	def __str__(self):
		return '{0}:{1} - {2}:{3}'.format(self.x1, self.y1, self.x2, self.y2)


class SolitairSolver(object):
	def __init__(self, field):
		self.field = field
		self.moveStack = []

		self.fieldHashes = {}

		self.solutionCount = 0
		self.cacheHit = 0
		self.cacheHitDepth = 0
		self.iterationCount = 0

		self.maxIterations = 0


	def inCache(self):
		hashTuple = self.field.getTwoLevelHash()
		h1, h2 = hashTuple

		if h1 in self.fieldHashes:
			return (h2 in self.fieldHashes[h1], hashTuple)

		return (False, hashTuple)


	def addToCache(self):
		h1, h2 = self.field.getTwoLevelHash()

		if not h1 in self.fieldHashes:
			self.fieldHashes[h1] = set()

		self.fieldHashes[h1].add(h2)

	def addHashToCache(self, hashTuple):
		h1, h2 = hashTuple

		if not h1 in self.fieldHashes:
			self.fieldHashes[h1] = set()

		self.fieldHashes[h1].add(h2)


	def solve(self, stopOnFind, maxIterations):
		self.solutionCount = 0
		self.cacheHit = 0
		self.iterationCount = 0

		self.maxIterations = maxIterations

		self.recursiveStep()

		#print(self.cacheHit)
		#print(self.iterationCount)


	def recursiveStep(self):
		if self.iterationCount > self.maxIterations:
			return

		if self.field.isComplete():
			self.solutionCount += 1
			print('Solved ' + str(self.iterationCount))
			for m in stack:
				print(' ' + m)

		hit, hashTuple = self.inCache()
		if not hit:
			if self.field.isSolvable:
				moves = self.field.getMoves()
				if len(moves) != 0:
					for move in moves:
						self.field.move(move)
						self.moveStack.append(move)
						self.recursiveStep()

				self.addHashToCache(hashTuple)
				#time.sleep(0.01)
		else:
			self.cacheHit += 1
			self.cacheHitDepth += len(self.moveStack)


		lastMove = self.moveStack.pop()
		self.field.reverse(lastMove)

		self.iterationCount += 1

		# print statistics
		if self.iterationCount % 10000 == 0:
			print('{},{},{}'.format(self.cacheHit, self.cacheHitDepth, len(self.fieldHashes)))
			self.cacheHitDepth = 0
			self.cacheHit = 0




class FieldDrawer(object):
	def __init__(self, field):
		self.field = field
		self.fieldHashes = {}

		self.draw()

	def draw(self):
		self.window = tkinter.Tk()

		self.labelsText = []
		for i in range(81):
			self.labelsText.append(tkinter.StringVar())

		self.piecesCount = tkinter.StringVar()
		self.iterationCount = tkinter.IntVar(0)
		self.solutionCount = tkinter.IntVar(0)

		for r in range(9):
			for c in range(9):
				self.labelsText[r * 9 + c].set(self.field.isOccupied(c, r))
				tkinter.Label(self.window,
					textvariable=self.labelsText[r * 9 + c],
					bg='white' if self.field.isField(c, r) else '#777777',
					width=3, height=2,
					borderwidth=1).grid(row=r,column=c)

		tkinter.Label(self.window,
			textvariable=self.piecesCount,
			bg='white',
			width=3, height=2,
			borderwidth=1).grid(row=10,column=0)

		tkinter.Label(self.window,
			textvariable=self.iterationCount,
			bg='white',
			width=12, height=2,
			borderwidth=1).grid(row=10, column=1, columnspan=4)

		tkinter.Label(self.window,
			textvariable=self.solutionCount,
			bg='white',
			width=12, height=2,
			borderwidth=1).grid(row=10, column=6, columnspan=2)

		self.moveListbox = tkinter.Listbox(self.window, height=45)
		self.moveListbox.grid(row=0,column=10,rowspan=45)

		_thread.start_new_thread(self.run, ())
		self.window.mainloop()

	def drawField(self):
		for r in range(9):
			for c in range(9):
				self.labelsText[r * 9 + c].set(self.field.isOccupied(c, r))


	def inCache(self):
		hashTuple = self.field.getTwoLevelHash()
		h1, h2 = hashTuple

		if h1 in self.fieldHashes:
			return (h2 in self.fieldHashes[h1], hashTuple)

		return (False, hashTuple)


	def addToCache(self):
		h1, h2 = self.field.getTwoLevelHash()

		if not h1 in self.fieldHashes:
			self.fieldHashes[h1] = set()

		self.fieldHashes[h1].add(h2)

	def addHashToCache(self, hashTuple):
		h1, h2 = hashTuple

		if not h1 in self.fieldHashes:
			self.fieldHashes[h1] = set()

		self.fieldHashes[h1].add(h2)


	def move(self, stack):
		if self.field.isComplete():
			self.solutionCount.set(self.solutionCount.get() + 1)
			print('Solved ' + str(self.iterationCount.get()))
			for m in stack:
				print(m)

		self.drawField()
		self.piecesCount.set(self.field.numberOfPieces())
		self.iterationCount.set(self.iterationCount.get() + 1)

		# performance testing
		#if self.iterationCount.get() > 20000:
		#	print(sys.getsizeof(self.fieldHashes) + len(self.fieldHashes) * sys.getsizeof(self.fieldHashes[0]))
		#	print(len(self.fieldHashes))
		#	_thread.interrupt_main()
		# performance testing end

		hit, hashTuple = self.inCache()
		if not hit:
			if self.field.isSolvable:
				moves = self.field.getMoves()
				if len(moves) != 0:
					for move in moves:
						self.field.move(move)
						stack.append(move)
						self.moveListbox.insert(tkinter.END, move)
						self.move(stack)

				self.addHashToCache(hashTuple)
				#time.sleep(0.01)
		else:
			#print('cache hit ' + str(self.iterationCount.get()))
			pass


		lastMove = stack.pop()
		self.field.reverse(lastMove)
		self.moveListbox.delete(len(self.moveListbox.get(0, tkinter.END)) - 1)

		return


	def run(self):
		self.move([])




if __name__ == '__main__':
	field = Field()
	#m1 = Move(2, 4, 4, 4)
	#field.move(m1)
	#print(field.getHash())
	#window = FieldDrawer(field)
	solver = SolitairSolver(field)
	solver.solve(True, 1000000)
	#cProfile.run('solver.solve(True, 100000)')

